# Creating Lists

Lists can be created in two ways

1. **CSV Upload**

   Any data can be uploaded in a format provided to create lists. The following headers are allowed:

   Email, First Name, Last Name, City, Country, Birthday, Anniversary, Gender, Check-in Date, Check-out Date, Room Type, Adults, Children, Booking Source, Guest Type, Room No

    

2. **Guest Database Search**

   - **All Guest Data**: Search All Geusts Data coming from PMS (This constitues of all reservations/bookings): Booking Source, Check-in Date, Check-out Date, Adult Count, Child Count, Booking Confirmation #, Room #, Reservation State, City, Country, Email, Name, Phone Number, State, Room Category, Nights Stayed, Total Revenue, Anniversary, Birthday, Preferences (as configured)
   - **Feedback Data**: Search Data of Guests who have completed feedback form: Individual Ratings
   - **Direct Upload**: Search from lists uploaded from CSVs (Depending on which fields were selected at the time of upload):  Email, First Name, Last Name, City, Country, Birthday, Anniversary, Gender, Check-in Date, Check-out Date, Room Type, Adults, Children, Booking Source, Guest Type, Room No

   

   **Note**: created from Database can be chosen to update dynamically based on the search criteria. For example, if a list search criteria is all Guests whose Booking source is Booking.com, the list will keep updating as new guests make a booking from Booking.com

# List Upload/Creation Process

Lists uploaded as CSVs go through an approval process to check for bounced and invalid entries. Bounces are checked from **neverbounce.com** and any lists with more than 5% est. bounce rate are automatically rejected.

Once lists pass the bounce test, they need to be manually approved before they can be used for sending campaigns.

# Creating Campaigns

We are trying to place the documentation system here

This is how you create Lists



# Unsubscribe

Unsubscription can happen in two ways:

1. **Promotional Email Unsubscription**: All Emails that are classified as Promotional (All Marketing Campaigns, Birthday Campaigns, Anniversary Campaigns, Bringback Emails)
2. **Transactional Email Unsubscription**: All Emails that are qualified as Transactional (Booking Confirmation, Feedback Invite, Pre-arrival welcome, check-in confirmation, Review Invite)

A contact can be unsubscribed in two ways:

1. **Email Unsubscribe**: An unsubscribe link is embedded in every email that goes out to guests. They can click on the link and choose to end subscription from either promotional emails, or transactional emails or both.
2. **Manual Unsubscribe**: An Xperium user can manually choose to unsubscribe an email (from a list) or CRM Guest from either promotional emails, or transactional emails or both.

Global Unsubscribers List:

1. **Promotional Email Unsubscribers List**: All Email Addresses that have unsubscribed from promotional emails either from an email or manually by a user

2. **Transactional Email Unsubscribers List**: All Email Addresses that have unsubscribed from transactional emails either from an email or manually by a user

   An Email address can exist in both unsubscriber lists at the same time. In such case, no email should be sent to the user from RepUp system unless it is re-subscribed.

**How will unsubscribed entries be handled**

While sending a campaign, all of its contacts would be matched against promotional or transactional unsubscribers list based on the type of campaign, and the unsubscribes identified. This number will be sent to deliveries with unsubscribe tag for stats calculation.

# Archive Lists & Campaigns

**Archived Lists**: Any search based or uploaded lists can be archived. Such archived lists cannot be reused for sending campaigns.

**Archived Campaigns**: Any campaigns in draft or previously sent can be archived. Such campaigns cannot be reused, duplicated or edited. However, stats would be available for any campaigns that have been sent previously.

**Sending Campaigns to Archived Lists**

1. Archived lists would not be available for selection at the time of sending a campaign. 
2. If a campaign saved as draft or a duplicate campaign contains an archived list, the campaign cannot be sent without removing that list and selecting some other list
3. If a campaign is scheduled to be sent at a particular time and contains an archived list, it would be sent anyway.

# Sending Campaigns at a Specific Time

We are trying to place the documentation system here

This is how you create Lists