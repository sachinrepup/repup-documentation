Guest journey defines how communications are delivered to guests (or guest segments) based on a pre-defined list of schedules.

## Schedule Creation Criteria

Schedules can be created based on timestamps in the guest journey including 

1. Booking Timestamp (Date and time of when Booking is registered in Xperium)
2. Check-in Timestamp (Date and actual time-stamp of check-in)
3. Check-in Date (Date of Check-in)
4. Check-out Date (Date of Check-out)
5. Check-out Timestamp (Date and actual time of check-out)
6. Feedback Submission Timestamp (Date and actual time of Feedback submission)
7. Feedback Submission Date (Date of feedback submission)
8. Anniversary (Date of Guest Anniversary)
9. Birthday (Date of Guest Birthday)
10. Booking Cancellation Timestamp (Date and time of when Booking cancellation is received in Xperium)



## Available Schedules

Schedules can be created based on the above criteria as:

1. Immediately: Available for Booking Timestamp, Check-in Timestamp, Checkout Timestamp, Feedback Submission Timestamp
2. After (Days, Months, Hours): Available for all Timestamps/Dates
3. Before (Days, Months, Hours): Unavailable for Booking Confirmation Date/Timestamp, Feedback Submission Date/Timestamp, Check-in Timestamp, Check-out Timestamp
4. At a specific time after/before an event



## Transactional and Promotional Emails

Campaigns can either be classified as transactional or promotional. Email templates will be separated accordingly as follows:

**Transactional Email Predefined Templates**: 

- Booking Confirmation Email
- Pre-Arrival Welcome Email
- Check-in Confirmation Email
- In-Stay Feedback Email
- Feedback Invite Email
- TripAdvisor Review Invite Email
- Booking Cancellation Email



**Promotional Email Predefined Templates**:

- Birthday Greeting Email
- Bringback Email
- Anniversary Greeting Email



## Email Types

A reservation or reservation update will be checked for applicable schedules (based on the list/list segment it belongs to. )

- **Booking Confirmation Email**: Default Schedule - immediately at booking (Do not send if Curr Date>=Check-in Date, only exception being when Curr date=booking date=checkin date)
- **Pre-Arrival Welcome Email**: Default Schedule - 1 day before check-in at 10:00 Local Time
- **Check-in Confirmation Email**: Default Schedule - immediately at check-in 
- **In-Stay Feedback Email**: Default Schedule - 1 day after check-in at 10:00 Local Time
- **Feedback Invite Email**: Default Schedule - 1 day after check-out at 10:00 Local Time
- **TripAdvisor Review Invite Email**: Default Schedule - 2 days after feedback submission at 10:00 Local Time
- **Booking Cancellation Email**: Default Schedule - Immediately at Cancellation
- **Birthday Greeting Email**: Default Schedule - Immediately at Birthday 
- **Anniversary Greeting Email**: Default Schedule - Immediately at Anniversary
- **Bringback Email**: Default Schedule - 30 days after check-out at 10:00 Local Time



## **Handling Email Updates after Booking**

All emails supposed to be sent on curr date will be delivered at the time email address is updated.

All emails other than booking confirmation will be sent according to usual schedule regardless of when email is updated (the only exception being in cases where email is updated on the date of email schedule at a later time than the scheduled time of sending the email)

No emails will go out after a booking is cancelled (all schedules based on check-in, checkout, booking dates will stand cancelled)

- **Updated after Booking but before Check-in**: Send Booking confirmation, Send Pre-Checkin if schedule hasnt expired
- **Upadated at Check-in**: Send Check-in confirmation if curr date=check-in date
- **Updated after Check-in but before Check-out**: Send all emails according to schedule
- **Updated at Check-out**: Send all emails according to schedule
- **Updated after Check-out**: Send all emails according to schedule



## **Email Schedules**

Email schedules will work according to the exact time specified as [mentioned above](/a).

If an email is scheduled to be sent x days before or after check-in/check-out/booking date at y:00 hrs, the schedule will work as follows:

1. Determine local date of the event (check-in/check-out/booking)
2. Set the date of sending as event_date +/- x days
3. Set the time of sending as y:00 hrs on event_date +/- x days



## **Processing Schedules for Past Reservations**

- TripAdvisor Review Invite Emails
- Bringback Emails
- Feedback Invite Emails

In case of above emails, there will be an option to process schedule for past reservations. However, these schedules will work on exact time intervals, meaning reservations checked-out 60 days in advance wouldnt be sent an email that is scheduled to be sent 30 days after checkout.

This means if a user selects one of these emails as the template, on the schedule screen, they'll be able to select whether the schedule should be processed for applicable past reservations.

In this case, if a reservation created in the past is to be sent a bringback email as per the set schedule, the email will be sent accordingly. 

e.g. If a bringback email is created to be sent 60 days after checkout on 20th August 2019, All reservations created between 20th June 2019 and 20th August 2019 would have to be processed as per schedule.