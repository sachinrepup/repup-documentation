# Release logs


### [29th August 2019]

#### Integrations

- PMS integrations updated to get time of events like check-in, check-out, booking confirmation etc
- Cloudbeds Integration now sends feedback data back to cloudbeds guest profiles


#### Marketing & Guest Journey

- Unsubscribe now works separately for promotional and transactional campaigns. A user can be subscribed from either promotional or transactional campaigns or both, and the email delivery will work accordingly.
- Guest Journey Update: Emails can be scheduled to go out at a specific time, and can be classified as promotional or transactional emails
- New UI for Guest Journey


#### Upsells

- Upsell Additional Details field added with WYSIWYG editor, if added, a button for more details would be visible in upsell card, which will show additional details in a popup
- Upsells can be approved from emails received by hotel users
- **Price Model Changes**: This defines on what basis the upsell offer is priced

	- **Single Price**: Pricing model for all upsells with a fixed price and only one time purchase applicable.
	- **Price per Unit**: Pricing model for upsells that can be bought in multiple quantities. One can limit the maximum number of units guest can buy.
	- **Price per Person**:  Pricing model for upsells that are priced per person. One can limit the maximum number of people who can buy.
	- **Price per Night**: Pricing model for upsells that are priced per night. Nights are automatically picked from the reservation.
	- **Discount Offer**: All upsells that are just discount offers and a customer who avails the offer wouldn't need to pay anything to get that offer.
	- **Free**: Offers that are completely free of cost.

- **New Price Variables**: Upsells can be at a **fixed price**, or the price can be varied on the basis of **Room Category** and **Rate Plan**
- **Multiple Products in an upsell**: An upsell can now constitute of multiple products. e.g. A spa upsell can have 30-min, 60-min and 90-min massage; each priced differently at a fixed price, or variable price according to room category or rate plan.

	These products can have their own images uploaded and would be selected by the guest using a dropdown.
	
#### Reputation

- Users can now report incorrect sentiment or category classification detected in a review phrase by going to Reputation -> Reports -> Category Classification and clicking on any phrase

#### Internal

- New parameters for CS/Finance added in Customer onboarding, will have to be filled before an account can be created.	

### [2nd August 2019]

#### Marketing Campaigns

- While creating marketing campaign user can move to any screen by clicking navigation bar stages
- User can search on Marketing list by email-id





### [26th July 2019]

#### Surveys

- Feedback Feed will now have time along with feedback submitted date

#### Marketing Campaigns

- Campaign stats are now more improvised. Unsubscribe stats are added in campaign stats display
- Marketing overview screen stats are improvised for quick display and performance
- On marketing email creation final screen button text from Send changed to Next for consistency

#### Automated Campaigns

- Automated campaigns can be delivered on particular time according to campaign timezone
- Automated statistics screen improved with Unsubscribe stats 



### [20th July 2019]

#### Surveys

- Clients can now see the feedback for which Tripadvisor reviews collected through RepUp are matched according to Guest Name. 

#### Reputation

- Functionality to download propertywise report only for selected properties.
- Pagination across CRM and Marketing has been standardised.

#### Marketing

- Automated emails with Upgrades have improvised to support Microsoft Outlook better
- Ability to archive a specific campaign.
- Marketing lists cannot be archived if used in draft campaigns. This is to avoid broken workflows while trying to access a draft containing archived lists.
- Improved Marketing stats excel report. New stats have been added 
  - Revenue stats 
  - Multiple CTA stats

#### Miscellaneous

- Dashboard ticket can be configured from admin console directly

  

### [12th July, 2019]



#### Reputation

- Fixed FIlters on Reviews Feed to Freeze at the top while scrolling the feed

- Review filters on dashboard are now fixed even after navigating to another dashboard screens

  

#### Surveys

- New User Privilege is added in team user section **Feedback Feed**: Now client can restrict user access to be limited to feedback feed

- For Guest Survey campaigns sent on email, Guest room number will also get auto filled as user clicks on the link to fill-in feedback form

- Comments on survey forms are now visible in downloaded guest feedback data from dashboard. This is in addition to guest details and ratings previously downloadable

- Individual sub-ratings can now be deleted from Group ratings in survey forms

- Acceptance to Privacy Policy is added in Guest Data Export so as to have a valid proof of Guest permission to send campaigns etc.

  

#### Marketing

- Revenue tracking dashboard will now have reservation details along with every reservation entry created as a result of the campaign

- Only Reservations starting after Campaign start date would now be considered to be eligible for campaign revenue attribution

- Increased maximum limit of email templates to 100 per property

  

#### Integration

- Impala (Opera) integration has been optimised to avoid redundant and inconsistent reservations from getting updated on Xperium

#### CRM

- Properties can now  add labels in questions to display additional text such as terms and conditionsfor any questions asked in the Web-Checkin forms
- Property can put instructions for document uploads e.g. Acceptable ID Proofs, Number of IDs etc for Web-Checkin forms in Form Configuration
